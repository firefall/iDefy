package com.firefall.testkeyboard2;


import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.widget.ImageView;
import android.content.ContentResolver;
import android.net.Uri;
import android.support.v4.content.FileProvider;

import java.io.File;


/**
 * Created by aric on 3/3/17.
 */

public class StorageImageButton extends ImageView {

    private Uri uri;
    private File file;

    private String AUTHORITY;
    public static File imagesDirectory;

    public StorageImageButton(Context context) {
        super(context);

        this.setBackgroundColor(Color.TRANSPARENT);

    }

    /**
     * @function setImageResource
     * @description Overrides ImageButton setImageResource, this function gets the resource ID and gets the URI from it to be used later
     * @return void
     */
    public void setImageResource(int resId, @NonNull File file) {
        Context context = this.getContext();

        uri = new Uri.Builder()
            .scheme(ContentResolver.SCHEME_ANDROID_RESOURCE)
            .authority(context.getResources().getResourcePackageName(resId))
            .appendPath(context.getResources().getResourceTypeName(resId))
            .appendPath(context.getResources().getResourceEntryName(resId))
            .build();

        AUTHORITY = context.getResources().getResourcePackageName(resId);

        this.file = file;
        super.setImageResource(resId);
    }

    /**
     * @function getUri
     * @description Returns the URI to the image resource being used for the button
     * @return
     */
    public Uri getUri() {
        return this.uri;
    }

    public Uri getFileUri() {
        return FileProvider.getUriForFile(this.getContext().getApplicationContext(), "com.firefall.fileprovider", file);
    }

}
