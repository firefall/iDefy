package com.firefall.testkeyboard2;

import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.BaseAdapter;
import android.content.Context;
import android.widget.GridView;
import android.widget.ImageButton;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by aric on 3/2/17.
 */

public class ImageButtonAdapter extends BaseAdapter {
    private Context context;
    private int[] imgIDs = {};
    private ArrayList<File> files;

    public ImageButtonAdapter(Context context, int[] imgIDs, ArrayList<File> files) {
        this.context = context;
        this.imgIDs = imgIDs;
        this.files = files;
    }

    @Override
    public int getCount() {
        return imgIDs.length;
    }

    @Override
    public Object getItem(int position) {
        return imgIDs[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int width = ((View) parent).getWidth();

        StorageImageButton imageButton = new StorageImageButton(context);
        imageButton.setImageResource(imgIDs[position], files.get(position));
        imageButton.setScaleType(ImageButton.ScaleType.CENTER_CROP);
        imageButton.setLayoutParams(new GridView.LayoutParams(width/3, width/3));

        return imageButton;
    }


}
