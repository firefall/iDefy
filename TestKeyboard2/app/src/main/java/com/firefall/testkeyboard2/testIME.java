package com.firefall.testkeyboard2;

import android.app.AppOpsManager;
import android.content.ClipDescription;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RawRes;
import android.support.v13.view.inputmethod.EditorInfoCompat;

import android.support.v13.view.inputmethod.InputConnectionCompat;
import android.support.v13.view.inputmethod.InputContentInfoCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.media.AudioManager;
import android.inputmethodservice.InputMethodService;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.ExtractedTextRequest;
import android.view.inputmethod.InputBinding;
import android.view.inputmethod.InputConnection;
import android.inputmethodservice.KeyboardView;
import android.inputmethodservice.Keyboard;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.GridView;
import android.content.Context;
import android.view.WindowManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import static android.content.ContentValues.TAG;

/**
 * Created by aric on 3/1/17.
 */

public class testIME extends InputMethodService implements KeyboardView.OnKeyboardActionListener {

    private Context context;

    // Keyboard Container
    private LinearLayout keyboardContainer;
    private KeyboardView keyboardView;

    // Sticker Container
    private Button stickerToggle;
    private GridView stickerContainer;

    private String AUTHORITY;
    private File imagesDirectory;

    private final int[] resIDs = {
            R.drawable.sticker_1_idefy_logo,
            R.drawable.sticker_2_idefy_fist,
            R.drawable.sticker_3_idefy_homo_trans_xeno_phobia,
            R.drawable.sticker_4_pink_lips_scream,
            R.drawable.sticker_5_idefy_racism,
            R.drawable.sticker_6_abortion_stigma,
            R.drawable.sticker_7_girl_pink_hair_wheelchair,
            R.drawable.sticker_8_girl_brown_hair_standing,
            R.drawable.sticker_9_girl_black_hair_standing,
            R.drawable.sticker_10_girl_red_hair_standing,
            R.drawable.sticker_11_purple_lips_closed,
            R.drawable.sticker_12_red_lips_smiling,
            R.drawable.sticker_13_yellow_lips_talking,
            R.drawable.sticker_14_brown_lips_closed,
            R.drawable.sticker_15_green_lips_parted,
            R.drawable.sticker_16_red_lips_laughing,
            R.drawable.sticker_17_blue_lips_kissing,
            R.drawable.sticker_18_tampon_with_sparkles,
            R.drawable.sticker_19_guy_orange_hair_standing_no_shadow
    };

    private ArrayList<File> imageFiles = new ArrayList<File>();

    // Flags
    private boolean caps = false;               // Flag for caps
    private boolean symbolsActive = false;      // Flag for symbols;
    private boolean stickerOpen = false;        // Flag for sticker drawer open or not
    private boolean supportGif = false;         // Flag for png support
    private boolean stickersEnabled = false;

    @Override
    public void onStartInputView(EditorInfo info, boolean restarting) {

        InputConnection inputConnection = getCurrentInputConnection();
        String[] mimeTypes = EditorInfoCompat.getContentMimeTypes(info);
        Keyboard keyboardNormal = new Keyboard(this, R.layout.qwerty);
        ExtractedText currentText;

        stickerOpen = false;
        caps = false;
        symbolsActive = false;
        // Set Visibility
        keyboardContainer.setVisibility(stickerOpen ? View.GONE : View.VISIBLE);
        stickerContainer.setVisibility(stickerOpen ? View.VISIBLE : View.GONE);

        // Set Text
        stickerToggle.setText(stickerOpen ? R.string.button_text_keyboard : R.string.button_text_sticker);

        // (Re)Verify that the input supports gif images
        supportGif = false;
        for(String mimeType : mimeTypes) {
            System.out.println("mimeType " + mimeType);
            if(ClipDescription.compareMimeTypes(mimeType, "image/gif")) {
                supportGif = true;
                System.out.println("Gif Support Set");
            }
        }

        if (!validatePackageName(info)) {
            supportGif = false;
        }

        stickersEnabled = supportGif;

        stickerToggle.setBackgroundResource(supportGif ? R.drawable.button_background_enabled : R.drawable.button_background_disabled);

        if(inputConnection != null) {
            currentText = inputConnection.getExtractedText(new ExtractedTextRequest(), 0);
            // Field must be empty of text otherwise setting images may degrade user experience
            if (currentText != null && currentText.text.length() != 0) {
                stickersEnabled = false;
                stickerToggle.setBackgroundResource(!supportGif || !stickersEnabled ? R.drawable.button_background_disabled : R.drawable.button_background_enabled);
            } else {
                stickersEnabled = true;
                stickerToggle.setBackgroundResource(supportGif && stickersEnabled ? R.drawable.button_background_enabled : R.drawable.button_background_disabled);
            }
        }


        keyboardView.setKeyboard(keyboardNormal);
    }

    @Override
    public View onCreateInputView() {

        final testIME _this = (testIME) this;
        View view = (View) getLayoutInflater().inflate(R.layout.keyboardlayout, null);
        DisplayMetrics metrics = new DisplayMetrics();
        ExtractedText currentText;
        File file;

        // Get the current input context (usually a field)
        final InputConnection inputConnection = getCurrentInputConnection();
        final EditorInfo editorInfo = getCurrentInputEditorInfo();

        // Set keyboard variable(s)
        keyboardContainer = (LinearLayout) view.findViewById(R.id.keyboardContainer);
        keyboardView = (KeyboardView) view.findViewById(R.id.keyboardview);

        Keyboard keyboardNormal = new Keyboard(this, R.layout.qwerty);

        // Set sticker variable(s)
        stickerContainer = (GridView) view.findViewById(R.id.stickerContainer);

        // Set button variable(s)
        stickerToggle = (Button) view.findViewById(R.id.imageToggle);

        ///////////////////////////
        // BEGIN: Init
        ///////////////////////////

        // Hide/Show keyboard/sticker drawer based on stickerOpen flag
        stickerOpen = false;
        keyboardContainer.setVisibility(stickerOpen ? View.GONE : View.VISIBLE);
        stickerContainer.setVisibility(stickerOpen ? View.VISIBLE : View.GONE);

        // Set Text
        stickerToggle.setText(stickerOpen ? R.string.button_text_keyboard : R.string.button_text_sticker);

        keyboardContainer.invalidate();
        stickerContainer.invalidate();

        // Look for the imageToggle button
        if(stickerToggle != null) {
            stickerToggle.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    // Must have gif support
                    if(!supportGif || !stickersEnabled)
                        return;

                    stickerOpen = !stickerOpen;

                    // Hide/Show keyboard/sticker drawer based on stickerOpen flag
                    keyboardContainer.setVisibility(stickerOpen ? View.GONE : View.VISIBLE);
                    stickerContainer.setVisibility(stickerOpen ? View.VISIBLE : View.GONE);

                    // Set Text
                    stickerToggle.setText(stickerOpen ? R.string.button_text_keyboard : R.string.button_text_sticker);
                }
            });
        }

        // Set up sticker Grid

        // Create files, these will be share
        imagesDirectory = new File(this.getFilesDir(), "images");
        imagesDirectory.mkdirs();

        for(int resID : resIDs) {
            file = getFileForResource(this, (+ resID), imagesDirectory, getApplicationContext().getResources().getResourceEntryName(resID) + ".gif");
            imageFiles.add(file);
        }

        LinearLayout.LayoutParams stickerContainerLayoutParams= (LinearLayout.LayoutParams) stickerContainer.getLayoutParams();
        ((WindowManager) this.getSystemService(this.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(metrics);
        stickerContainerLayoutParams.height = (int)(metrics.heightPixels*0.4);

        stickerContainer.setLayoutParams(stickerContainerLayoutParams);
        stickerContainer.setAdapter(new ImageButtonAdapter(this, resIDs, imageFiles));

        stickerContainer.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                final InputContentInfoCompat inputContentInfo;
                StorageImageButton button = (StorageImageButton) view;
                Uri uri = button.getFileUri();
                int flags = 0;
                boolean contentCommitted = false;
                InputConnection inputConnection = getCurrentInputConnection();
                EditorInfo editorInfo = getCurrentInputEditorInfo();
                ExtractedText currentText;

                if(!supportGif)
                    return;

                currentText = inputConnection.getExtractedText(new ExtractedTextRequest(), 0);
                // Field must be empty of text otherwise setting images may degrade user experience
                if(currentText != null && currentText.text.length() != 0) {
                    return;
                }

                if (android.os.Build.VERSION.SDK_INT >= 25) {
                    flags = InputConnectionCompat.INPUT_CONTENT_GRANT_READ_URI_PERMISSION;
                }else {

                    try {
                        _this.grantUriPermission(
                                editorInfo.packageName, uri, Intent.FLAG_GRANT_READ_URI_PERMISSION);}
                    catch(Exception e){
                        Log.e("Error", "grantUriPermission failed packageName=" + editorInfo.packageName
                                + " contentUri=" + uri, e);
                    }
                }

                inputContentInfo = new InputContentInfoCompat(
                        uri,
                        new ClipDescription("#iDefy", new String[]{"image/gif"}),
                        null
                );

                InputConnectionCompat.commitContent(getCurrentInputConnection(), getCurrentInputEditorInfo(), inputContentInfo, flags, null);
            }
        });

        if(inputConnection != null) {
            currentText = inputConnection.getExtractedText(new ExtractedTextRequest(), 0);

            // Field must be empty of text otherwise setting images may degrade user experience
            if (currentText != null && currentText.text.length() != 0) {
                stickersEnabled = false;
                stickerToggle.setBackgroundResource(!supportGif || !stickersEnabled ? R.drawable.button_background_disabled : R.drawable.button_background_enabled);
            } else {
                stickersEnabled = true;
                stickerToggle.setBackgroundResource(supportGif && stickersEnabled ? R.drawable.button_background_enabled : R.drawable.button_background_disabled);
            }
        }

        keyboardView.setKeyboard(keyboardNormal);
        keyboardView.setOnKeyboardActionListener(this);

        return view;
    }

    private void playClick(int keyCode){

        AudioManager am = (AudioManager)getSystemService(AUDIO_SERVICE);

        switch(keyCode){
            case 32:
                am.playSoundEffect(AudioManager.FX_KEYPRESS_SPACEBAR);
                break;
           // case Keyboard.KEYCODE_DONE;

            case 10:
                am.playSoundEffect(AudioManager.FX_KEYPRESS_RETURN);
                break;
            case Keyboard.KEYCODE_DELETE:
                am.playSoundEffect(AudioManager.FX_KEYPRESS_DELETE);
                break;
            default: am.playSoundEffect(AudioManager.FX_KEYPRESS_STANDARD);
        }
    }

    @Override
    public void onPress(int i) {

    }

    @Override
    public void onRelease(int i) {

    }

    @Override
    public void onKey(int i, int[] ints) {
        InputConnection inputConnection = getCurrentInputConnection();
        EditorInfo editorInfo = getCurrentInputEditorInfo();
        Keyboard keyboardNormal = new Keyboard(this, R.layout.qwerty);
        Keyboard keyboardSymbols= new Keyboard(this, R.layout.symbols);
        Keyboard keyboardSymbolsShifted = new Keyboard(this, R.layout.symbols_shift);
        ExtractedText currentText;
        char code;

        playClick(i);

        switch(i) {
            case Keyboard.KEYCODE_DELETE:
                inputConnection.deleteSurroundingText(1, 0);           // Deletes from the current position in text
                break;
            case Keyboard.KEYCODE_SHIFT:
                caps = !caps;
                if(!symbolsActive) {
                    keyboardView.getKeyboard().setShifted(caps);                              // Assumed to set shifted (capitalized) state
                    keyboardView.invalidateAllKeys();                       // Forces keys to be redrawn
                } else
                    keyboardView.setKeyboard(caps ? keyboardSymbolsShifted : keyboardSymbols);

                break;
            case Keyboard.KEYCODE_DONE:
                this.requestHideSelf(0);
                break;
            case Keyboard.KEYCODE_MODE_CHANGE:
                caps = false;
                symbolsActive = !symbolsActive;
                keyboardView.setKeyboard(symbolsActive ? keyboardSymbols : keyboardNormal);

                break;
            case 10:
                inputConnection.sendKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER));
                break;

            default:
                code = (char) i;
                if(Character.isLetter(code) && caps) {
                    code = Character.toUpperCase(code);
                }
                inputConnection.commitText(String.valueOf(code), 1);    // Put new text into input field
                break;
        }

        currentText = inputConnection.getExtractedText(new ExtractedTextRequest(), 0);

        // Field must be empty of text otherwise setting images may degrade user experience
        if(currentText != null && currentText.text.length() != 0) {
            stickersEnabled = false;
            stickerToggle.setBackgroundResource(!supportGif || !stickersEnabled ? R.drawable.button_background_disabled : R.drawable.button_background_enabled);
        } else {
            stickersEnabled = true;
            stickerToggle.setBackgroundResource(supportGif && stickersEnabled ? R.drawable.button_background_enabled : R.drawable.button_background_disabled);
        }
    }

    @Override
    public void onText(CharSequence charSequence) {

    }

    @Override
    public void swipeLeft() {

    }

    @Override
    public void swipeRight() {

    }

    @Override
    public void swipeDown() {

    }

    @Override
    public void swipeUp() {

    }

    private static File getFileForResource(
            @NonNull Context context, @RawRes int res, @NonNull File outputDir,
            @NonNull String filename) {
        final File outputFile = new File(outputDir, filename);
        final byte[] buffer = new byte[4096];
        InputStream resourceReader = null;
        try {
            try {
                resourceReader = context.getResources().openRawResource(res);
                OutputStream dataWriter = null;
                try {
                    dataWriter = new FileOutputStream(outputFile);
                    while (true) {
                        final int numRead = resourceReader.read(buffer);
                        if (numRead <= 0) {
                            break;
                        }
                        dataWriter.write(buffer, 0, numRead);
                    }
                    return outputFile;
                } finally {
                    if (dataWriter != null) {
                        dataWriter.flush();
                        dataWriter.close();
                    }
                }
            } finally {
                if (resourceReader != null) {
                    resourceReader.close();
                }
            }
        } catch (IOException e) {
            return null;
        }
    }

    private boolean validatePackageName(@Nullable EditorInfo editorInfo) {
        if (editorInfo == null) {
            return false;
        }
        final String packageName = editorInfo.packageName;
        if (packageName == null) {
            return false;
        }

        // In Android L MR-1 and prior devices, EditorInfo.packageName is not a reliable identifier
        // of the target application because:
        //   1. the system does not verify it [1]
        //   2. InputMethodManager.startInputInner() had filled EditorInfo.packageName with
        //      view.getContext().getPackageName() [2]
        // [1]: https://android.googlesource.com/platform/frameworks/base/+/a0f3ad1b5aabe04d9eb1df8bad34124b826ab641
        // [2]: https://android.googlesource.com/platform/frameworks/base/+/02df328f0cd12f2af87ca96ecf5819c8a3470dc8
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return true;
        }

        final InputBinding inputBinding = getCurrentInputBinding();
        if (inputBinding == null) {
            // Due to b.android.com/225029, it is possible that getCurrentInputBinding() returns
            // null even after onStartInputView() is called.
            // TODO: Come up with a way to work around this bug....
            Log.e(TAG, "inputBinding should not be null here. "
                    + "You are likely to be hitting b.android.com/225029");
            return false;
        }
        final int packageUid = inputBinding.getUid();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            final AppOpsManager appOpsManager =
                    (AppOpsManager) getSystemService(Context.APP_OPS_SERVICE);
            try {
                appOpsManager.checkPackage(packageUid, packageName);
            } catch (Exception e) {
                return false;
            }
            return true;
        }

        final PackageManager packageManager = getPackageManager();
        final String possiblePackageNames[] = packageManager.getPackagesForUid(packageUid);
        for (final String possiblePackageName : possiblePackageNames) {
            if (packageName.equals(possiblePackageName)) {
                return true;
            }
        }
        return false;
    }


}
